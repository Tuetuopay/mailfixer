Mail Fixer for iOS 7
====================

This is a small utility meant to automatize the process of fixing the incompatibility between Cydia tweaks and Mail, Safari, Calculator, and Weather. So far, AppSync 7 and afc2add are the only known tweaks to cause that.

Getting Started
===============

You have two options : installing MailFixer to your device straight from my Cydia repo [here](http://tuetuopay.legtux.org/cydia/) or taking the journey of compiling it from source, sending it to your device and ssh’ing to allow root execution.

This app relies on some Cydia packages :

* `wget` (checking internet connexion)
* `apt7` (downloading the packages afc2add/AppSync)
* `bash`
* `coreutils` (whoami, cut, tr, ...)

From Cydia
----------

Seriously ? Do I really need to explain that ?

1. Open Cydia
2. Under « Manage » tab, go in « Repositories »
3. Tap « Add » and type « [http://cydia.tuetuopay.fr](http://cydia.tuetuopay.fr) », then hit « OK »
4. Browse into this newly added source and install « MailFixer »
5. Quit Cydia and open MailFixer
6. Follow instructions

From Source
-----------

Make sure you have Xcode 5 installed with iOS 7 SDK installed and make sure that OpenSSH is installed on your iDevice. In terminal, type
```sh
git clone http://opensource.tuetuopay.fr/mailfixer.git
cd mailfixer 
./compile
```

Open up your favorite FTP transfer program (FileZilla for example) and transfer the .app generated (`build/Release-iphoneos/MailFixer.app`) to your device in `/Applications` (and nowhere else !); the default password is `alpine` (I expect you to know what is your root password if you changed it). Now SSH into your device : open Terminal and type the following, where IP is your device’s IP address on your network (both iDevice & Mac needs to be on the same LAN)
```sh
ssh root@IP
```
Password will be prompted, type `alpine` (without quotes), nothing will show up when typing, this is _normal_ ; then hit enter. once in your device, type the following (DO NO SKIP THIS STEP : this is what allows the application to run as root !) :
```sh
cd /Applications/MailFixer.app/
chmod +x patch.sh
./patch.sh
```
Then to make the app appear :
```sh
killall SpringBoard
```
During the respring :
```sh
apt-get install wget apt7 bash coreutils
```
If the app still didn't appeared, reboot your device.

For Masochists
--------------

You can do everything without exiting your Terminal window. `$IP` is your device IP address on the same LAN than your computer, the default iDevice password is `alpine`.
```sh
git clone http://opensource.tuetuopay.fr/mailfixer.git
cd mailfixer 
./compile
# iDevice password will be prompted
scp -r build/Release-iphoneos/MailFixer.app root@$IP:/Applications
# iDevice password will be prompted
ssh root@$IP
# in the device root@iphone:~ #
cd /Applications/MailFixer.app/
chmod +x patch.sh
./patch.sh
killall SpringBoard
apt-get install wget apt7 bash coreutils
```

Voilà, ready to use my app :D