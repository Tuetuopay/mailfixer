//
//  MFAppDelegate.h
//  MailFixer
//
//  Created by Alexis on 29/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
