//
//  main.m
//  MailFixer
//
//  Created by Alexis on 29/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MFAppDelegate.h"

int main(int argc, char * argv[])
{
	/* Gaining root access on the device. Full procedure here :
	 * http://stackoverflow.com/questions/7841344/gaining-root-permissions-on-ios-for-nsfilemanager-jailbreak/8796556#8796556
	 * (this is the way Cydia becomes root)
	 */
	NSLog (@"setuid(0) = %d", setuid (0));
	NSLog (@"setgid(0) = %d", setgid (0));
	
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([MFAppDelegate class]));
	}
}
