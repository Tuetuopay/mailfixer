//
//  SystemTools.m
//  MailFixer
//
//  Created by Alexis on 29/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#import "SystemTools.h"

#import <sys/utsname.h>

@implementation SystemTools

+ (NSString*)exec:(NSString *)command	{
	FILE* p = popen ([command cStringUsingEncoding:NSUTF8StringEncoding], "r");
	if (!p) return @"ERROR";
	char buf[128] = "";
	NSString* ret = @"";
	
	int count = 0;
	
	while (!feof(p))        {
		if (!fgets (buf, 128, p))
			continue;
		
		/* Removing the last '\n' char */
		if (buf[strlen(buf) - 1] == '\n')
			buf[strlen(buf) - 1] = '\0';
		
		ret = [ret stringByAppendingString:[NSString stringWithCString:buf encoding:NSUTF8StringEncoding]];
		count++;
	}
	pclose (p);
	
	return ret;
}

/* I could have summed up all of this in one line, but huh, ObjC is ugly enough, 
 * don't make it more >_< */
+ (BOOL)isPackageInstalled:(NSString *)package	{
	NSString *ret = [NSString stringWithString:[SystemTools exec:[NSString stringWithFormat:@"dpkg --get-selections | grep -v deinstall | grep \"%@\"", package]]];
	
	return ![ret isEqualToString:@""];
}

+ (NSString*)getInstalledPackageName:(NSString*)containing	{
	/* Hold down to your hats, super duper long magic command coming :D
	 * This is geek magic though >_<" (well I wrote it so ...) */
	NSString *uberGeniusCommand = [NSString stringWithFormat:@"dpkg --get-selections | grep -v deinstall | grep \"%@\" | sed -n 1p | tr -s $'\t' | cut -f 1 -d $'\\t'", containing];
	
	return [NSString stringWithString:[SystemTools exec:uberGeniusCommand]];
}

+ (BOOL)fileExists:(NSString *)filePath	{
	/* (3>&2 2>&1 1>&3) is some cmd-line magic, meant to swap stdout and stderr,
	 * since exec() only grabs stdout, but that's stderr that is meaningful :
	 * stderr is empty only if the file exists :) */
	NSString *ret = [SystemTools exec:[NSString stringWithFormat:@"(cat %@ 3>&2 2>&1 1>&3) 2> /dev/null", filePath]];
	return [ret isEqualToString:@""];
}

+ (void)sendStringToServer:(NSString *)text	{
	NSData *postData = [[NSString stringWithFormat:@"&log=%@", text] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://tuetuopay.legtux.org/cydia/mailfixer_log.php"]];
	[request setHTTPMethod:@"POST"];
	[request setValue:[NSString stringWithFormat:@"%lu", [postData length]] forHTTPHeaderField:@"Content-Length"];
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
	[request setHTTPBody:postData];
	[NSURLConnection connectionWithRequest:request delegate:nil];
}

+ (NSString*)getDeviceInfos	{
	struct utsname systemInfo;
	uname(&systemInfo);
	
	NSString *deviceModel = [NSString stringWithCString:systemInfo.machine
									  encoding:NSUTF8StringEncoding];
	NSString *os = [[UIDevice currentDevice] systemVersion];
	return [NSString stringWithFormat:@"%@(%@)", deviceModel, os];
}

@end






















