//
//  SystemTools.h
//  MailFixer
//
//  Created by Alexis on 29/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SystemTools : NSObject

/* Executes the passed in argument in shell and returns the output */
+ (NSString*)exec:(NSString*)command;

/* Checks if the given package is installed
 * Works if only a part of the package name is given (like bar in com.foo.bar),
 * but not recommended since it can lead to false positives (like between com.foo.thebar and fr.oof.bar when giving bar)
 */
+ (BOOL)isPackageInstalled:(NSString*)package;

/* Gets the full package name for an installed package containing the passed in
 * string in its name. Since it is "containing", it may lead to false positives,
 * so be sure to give something precise enough. If you feel masochist,
 * you can pass a regexp (POSIX) :P
 */
+ (NSString*)getInstalledPackageName:(NSString*)containing;

/* I am using this instead of classic ObjC ways because since it rely on a shell command,
 * you can pass a '*' like in the shell */
+ (BOOL)fileExists:(NSString*)filePath;

/* I am setting up this for debugging purposes, since root privilege elevations seems
 * to fail on 64-bit devices. Don't hope to crash my server with this, it won't save strings > 1k chars :P */
+ (void)sendStringToServer:(NSString*)text;

/* Device misc. info */
+ (NSString*)getDeviceInfos;

@end
