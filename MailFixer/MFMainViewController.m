//
//  MFMainViewController.m
//  MailFixer
//
//  Created by Alexis on 29/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#import "MFMainViewController.h"

#import "SystemTools.h"
#import "DejalActivityView.h"

@interface MFMainViewController ()

@end

@implementation MFMainViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	_currentAlertView = ALERT_NONE;
	_forceNoInternet = NO;
	_isBusy = NO;
	
	/* #1 Thing to check : are we root ? */
	
	_amIRoot = YES;
	_user = [SystemTools exec:@"whoami"];	/* Basic unix stuff here */
	if (![_user isEqualToString:@"root"])	{
		/* Well I should disable everything but it's faster to do nothing when the user triggers the button */
		
		_amIRoot = NO;	/* :( */
		[[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error : '%@'", _user]
									message:@"The application couldn't start up as root, it will be the inefficient. Please contact the author."
									delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
		_currentAlertView = ALERT_NOT_ROOT;
		[self performSelectorInBackground:@selector(sendNotRootLogToServer:) withObject:nil];
	}
	
	/* All this stuff can be done as mobile, so I'm keeping that here */
	
	/* #2 : Is afc2add installed ? */
	if (![SystemTools isPackageInstalled:@"us.scw.afctwoadd"])
		[_switchAfc2add setEnabled:NO];
	else
		[_switchAfc2add setOn:YES];
	
	/* #3 : Is AppSync installed ? More tricky, since we need to retreive the installed appsync */
	/* Since we are on iOS 7, only the good appsync can be installed */
	_appSyncPackageName = [NSString stringWithString:[SystemTools getInstalledPackageName:@"appsync"]];
	if ([_appSyncPackageName isEqualToString:@""])	/* AppSync not installed => pack name empty */
		[_switchAppSync setEnabled:NO];
	else if (![SystemTools isPackageInstalled:_appSyncPackageName]) /* Let's double check xD */
		[_switchAppSync setEnabled:NO];
	else
		[_switchAppSync setOn:YES];
	
	_useAfc2add = [_switchAfc2add isOn];
	_useAppSync = [_switchAppSync isOn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath	{
	/* That's a static table view, so let's make hard-coded values ! */
	
	[_cellFix setSelected:NO animated:YES];
	
	if (indexPath.section == 2 && indexPath.row == 0)	{
		/* There the fun begins ! */
		
		if (!_amIRoot)	{
			/* I told you I won't work as mobile è_é */
			[[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error : '%@'", _user]
									message:@"The application couldn't start up as root, it will be the inefficient. Please contact the author."
									delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
			_currentAlertView = ALERT_NOT_ROOT;
			return;
		}
		
		[self beginTheMagic];
	}
}

- (IBAction)switchAfc2addChanged:(id)sender {
	_useAfc2add = [_switchAfc2add isOn];
}

- (IBAction)swichAppSyncChanged:(id)sender {
	_useAppSync = [_switchAppSync isOn];
}

- (void)beginTheMagic	{
	if (_isBusy)
		return;
	
	_isBusy = YES;
	
	[_switchAfc2add setEnabled:NO];
	[_switchAppSync setEnabled:NO];
	
	[DejalBezelActivityView activityViewForView:self.tableView.superview withLabel:@"Ze Magic" width:150];
	
	[self performSelectorInBackground:@selector(fixThatPhone:) withObject:nil];
}
- (void)fixThatPhone:(id)selector	{
	/* Since this is destined to be ran inside a thread, we need our own autorelease pool */
	@autoreleasepool {
		/* DISCLAIMER : this is dirty thread programming, but since this thread is only meant to not freeze the UI,
		 * we'll assume that nothing will happen during this time */
		if (![self downloadPackages])	/* Wow, so much shit happening in there */
			return;
		
		[self performSelectorOnMainThread:@selector(currentActivity:) withObject:@"Removing afc2add" waitUntilDone:NO];
		[SystemTools exec:@"yes | apt-get remove us.scw.afctwoadd > mailfixer.log 2>&1"];
		
		// sleep(2);
		
		[self performSelectorOnMainThread:@selector(currentActivity:) withObject:@"Removing AppSync 7" waitUntilDone:NO];
		[SystemTools exec:[NSString stringWithFormat:@"yes | apt-get remove %@  > mailfixer.log 2>&1", _appSyncPackageName]];
		
		// sleep(2);
		
		[self performSelectorOnMainThread:@selector(currentActivity:) withObject:@"Removing caches" waitUntilDone:NO];
		[SystemTools exec:@"rm /var/mobile/Library/Caches/com.apple.mobile.installation.plist /var/mobile/Library/Caches/com.apple.LaunchServices-054.csstore"];
		
		[self performSelectorOnMainThread:@selector(currentActivity:) withObject:@"Installing loadup" waitUntilDone:NO];
		[SystemTools exec:[NSString stringWithFormat:@"cp %@ /System/Library/LaunchDaemons/", [[NSBundle mainBundle] pathForResource:@"com.tuetuopay.mailfixer" ofType:@"plist"]]];
		[SystemTools exec:@"chown root:wheel /System/Library/LaunchDaemons/com.tuetuopay.mailfixer.plist"];
		/* Trivia : this chown gave me headache during 3 hours, because this file is owned by root:admin and not 
		 * root:wheel, so launchd won't take it in account >_<" */
		
		/* #!/bin/bash
		 * 
		 * rm /Library/LaunchDaemons/com.tuetuopay.mailfix.plist /usr/local/mailfixer.sh
		 * touch /var/mobile/mailfix.log
		 * sleep 30
		 * we are slleping because when the OS executes the LaunchDaemons, dpkg is still not fully functional,
		 * and I spent more than 5 hours thinking that my scrip wasn't executed because my testing AppSync
		 * didn't appeared as installed in apt-get >_>
		 * I got that dpkg was not ready when I found, after ... 15 reboots ? ... that AppSync files were actually
		 * present on the system. So for VERY slow devices, i'll increase delay to 30s (in my 4S, 15s are enough)
		 */
		[SystemTools exec:@"echo $'#!/bin/bash\n\nrm /System/Library/LaunchDaemons/com.tuetuopay.mailfixer.plist /usr/local/mailfixer.sh\ntouch /var/mobile/mailfix.log\nsleep 30' > /usr/local/mailfixer.sh"];
		if (_useAfc2add)
			[SystemTools exec:@"echo $'dpkg -i /var/mobile/Documents/us.scw.afctwoadd*.deb >> /var/mobile/mailfix.log 2>&1\n' >> /usr/local/mailfixer.sh"];
		if (_useAppSync)
			[SystemTools exec:[NSString stringWithFormat:@"echo $'dpkg -i /var/mobile/Documents/%@*.deb >> /var/mobile/mailfix.log 2>&1\n\n' >> /usr/local/mailfixer.sh", _appSyncPackageName]];
		[SystemTools exec:@"echo $'rm /usr/local/mailfixer.sh\n'"];
		[SystemTools exec:@"chmod 775 /usr/local/mailfixer.sh && chown root:wheel /usr/local/mailfixer.sh"];
		
		[self performSelectorOnMainThread:@selector(currentActivity:) withObject:@"Done, will reboot" waitUntilDone:NO];
		
		/* Let's give the user time to see this message */
		sleep(2);
		
		[SystemTools exec:@"reboot"];
		
		/* This is for debug purposes when the reboot is disabled */
		[self performSelectorOnMainThread:@selector(endedTheMagic:) withObject:nil waitUntilDone:NO];
	}
}
- (BOOL)downloadPackages	{
	/* First, let's check internet connexion */
	[self performSelectorOnMainThread:@selector(currentActivity:) withObject:@"Checking Internet" waitUntilDone:NO];
	if (!_forceNoInternet && ((_useAfc2add && ![SystemTools fileExists:@"/var/mobile/Documents/us.scw.afctwoadd*.deb"]) ||
							  (_useAppSync && ![SystemTools fileExists:[NSString stringWithFormat:@"/var/mobile/Documents/%@*.deb", _appSyncPackageName]])))	{
		[SystemTools exec:@"wget http://google.com -O /g.html"];
		/* Darn, wget is sending everything to stderr >_> */
		if ([[SystemTools exec:@"if [[ ! -s /g.html ]] ; then echo nope ; fi"] isEqualToString:@"nope"])	{	/* No internet */
			[[[UIAlertView alloc] initWithTitle:@"Hum" message:@"You don't seem to have any Internet connection. You will need to install the tweaks manually after reboot, or connect to the Internet now. Install anyway ?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Install anyway", nil] show];
			[self performSelectorOnMainThread:@selector(currentAlertView:) withObject:[NSNumber numberWithInt:ALERT_NO_INTERNET] waitUntilDone:NO];
			[self performSelectorOnMainThread:@selector(endedTheMagic:) withObject:nil waitUntilDone:NO];
			[SystemTools exec:@"rm /g.html"];
			return NO;
		} else	{	/* Internet \o/ */
			/* We still need to recheck for cached file. Use your brain, simple logic here on why */
			if (_useAfc2add && ![SystemTools fileExists:@"/var/mobile/Documents/us.scw.afctwoadd*.deb"])	{
				[self performSelectorOnMainThread:@selector(currentActivity:) withObject:@"Downloading afc2add" waitUntilDone:NO];
				[SystemTools exec:@"yes | apt-get install -d us.scw.afctwoadd --reinstall"];	/* --reinstall is necessary here since afc2add is installed */
				[SystemTools exec:@"mv /var/cache/apt/archives/us.scw.afctwoadd*.deb /var/mobile/Documents/"];
			}
			if (_useAppSync && ![SystemTools fileExists:[NSString stringWithFormat:@"/var/mobile/Documents/%@*.deb", _appSyncPackageName]])	{
				[self performSelectorOnMainThread:@selector(currentActivity:) withObject:@"Downloading AppSync 7" waitUntilDone:NO];
				[SystemTools exec:[NSString stringWithFormat:@"yes | apt-get install -d %@ --reinstall", _appSyncPackageName]];
				[SystemTools exec:[NSString stringWithFormat:@"mv /var/cache/apt/archives/%@*.deb /var/mobile/Documents/", _appSyncPackageName]];
			}
		}
	}
	
	[SystemTools exec:@"rm /g.html"];
	return YES;
}

- (void)currentActivity:(id)message	{
	if ([[NSString stringWithString:message] isEqualToString:@"Done"])
		[DejalBezelActivityView removeViewAnimated:YES];
	else
		[DejalBezelActivityView currentActivityView].activityLabel.text = [NSString stringWithString:message];
}
- (void)currentAlertView:(id)view	{
	_currentAlertView = (MFAlertViewID)[view integerValue];
}
- (void)endedTheMagic:(id)unused	{
	[_switchAfc2add setEnabled:YES];
	[_switchAppSync setEnabled:YES];
	_isBusy = NO;
	[DejalBezelActivityView removeViewAnimated:YES];
}

/* Alert views handler */
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (_currentAlertView == ALERT_NO_INTERNET && buttonIndex == 1)	{
		_forceNoInternet = YES;
		[self beginTheMagic];
	}
	
	_currentAlertView = ALERT_NONE;
}

- (void)sendNotRootLogToServer:(id)unused	{
	@autoreleasepool {
		NSString *user = [NSString stringWithString:_user];
		[SystemTools sendStringToServer:[NSString stringWithFormat:@"%@: Not started as root (%@). App output:\n{\"%@\"}",
										 [SystemTools getDeviceInfos],
										 user,
										 [NSString stringWithContentsOfFile:@"/tmp/mailfixer.log" encoding:NSASCIIStringEncoding error:nil]]];
	}
}

@end
