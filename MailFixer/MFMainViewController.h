//
//  MFMainViewController.h
//  MailFixer
//
//  Created by Alexis on 29/12/2013.
//  Copyright (c) 2013 Tuetuopay. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	ALERT_NONE, ALERT_NOT_ROOT, ALERT_NO_INTERNET
} MFAlertViewID;

@interface MFMainViewController : UITableViewController

/* Outlets */
@property (weak, nonatomic) IBOutlet UISwitch *switchAfc2add;
@property (weak, nonatomic) IBOutlet UISwitch *switchAppSync;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellFix;
/* Actions */
- (IBAction)switchAfc2addChanged:(id)sender;
- (IBAction)swichAppSyncChanged:(id)sender;

/* General */
@property BOOL useAfc2add;
@property BOOL useAppSync;
@property BOOL amIRoot;
@property BOOL forceNoInternet;
@property MFAlertViewID currentAlertView;
@property BOOL isBusy;
/* Contains the technical name of appsync (com.somedude.xxappsync7yy) */
@property (strong, nonatomic) NSString* appSyncPackageName;

@property (strong, nonatomic) NSString* user;

- (BOOL)downloadPackages;	/* NO if error & stops; YES if everything good */

/* And this is where the magc happens */
/* The thread launcher */
- (void)beginTheMagic;
/* The threaded one */
- (void)fixThatPhone:(id)selector;

/* Thread-safe methods */
- (void)currentActivity:(id)message;	/* id <=> NSString* */
- (void)currentAlertView:(id)view;		/* id <=> MFAlertViewID */
- (void)endedTheMagic:(id)unused;

/* Debug */
- (void)sendNotRootLogToServer:(id)unused;

@end
